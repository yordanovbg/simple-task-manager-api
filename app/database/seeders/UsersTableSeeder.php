<?php

namespace Database\Seeders;

use App\Models\Users\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'email' => 'task_manager@mail.com',
            'password' => '123456',
            'task_manager' => true,
        ]);

        User::factory()->create([
            'name' => 'Jhon',
            'surname' => 'Doe',
            'email' => 'jhon.doe@mail.com',
            'password' => '123456',
            'task_manager' => false,
        ]);

        User::factory()->create([
            'name' => 'David',
            'surname' => 'Wayne',
            'email' => 'david.wayne@mail.com',
            'password' => '123456',
            'task_manager' => false,
        ]);
    }
}
