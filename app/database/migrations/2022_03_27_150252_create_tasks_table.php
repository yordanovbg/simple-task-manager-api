<?php

use App\Models\Tasks\Task;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->enum('status', [Task::STATUSES])
                ->default(Task::OPEN_STATUS)
                ->index();
            $table->unsignedBigInteger('assignee_id')
                ->nullable();
            $table->dateTime('assigned_at')
                ->nullable();
            $table->unsignedBigInteger('creator_id');
            $table->timestamps();
        });

        Schema::table('tasks', function (Blueprint $table) {
            $table->foreign('assignee_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
            $table->foreign('creator_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
};
