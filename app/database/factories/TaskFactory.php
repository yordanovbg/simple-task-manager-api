<?php

namespace Database\Factories;

use App\Models\Tasks\Task;
use App\Models\Users\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word(),
            'description' => $this->faker->text(),
            'status' => Task::OPEN_STATUS,
            'assignee_id' => User::factory()->create()->id,
            'assigned_at' => Carbon::now(),
            'creator_id' => User::factory()->create()->id,
        ];
    }
}
