<?php

namespace App\Models\Tasks;

use App\Models\Users\User;
use function auth;
use Database\Factories\TaskFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

class Task extends Model
{
    use HasFactory;

    public const STATUSES = [
        self::OPEN_STATUS, self::IN_PROGRESS_STATUS, self::COMPLETED_STATUS,
    ];

    public const OPEN_STATUS = 'open';
    public const IN_PROGRESS_STATUS = 'in_progress';
    public const COMPLETED_STATUS = 'completed';

    protected $dates = [
        'assigned_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'title', 'description', 'status', 'assignee_id', 'assigned_at', 'creator_id',
    ];

    public function assignee(): BelongsTo
    {
        return $this->belongsTo(User::class, 'assignee_id', 'id');
    }

    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    // Model dependencies
    protected static function newFactory()
    {
        return TaskFactory::new();
    }

    // Scopes

    public function scopeUserVisible(Builder $query): void
    {
        $query->when(! auth()->user()->isTaskManager(), function ($query) {
            $query->where(function ($query) {
                $query->where('assignee_id', auth()->id())
                    ->orWhere('creator_id', auth()->id());
            });
        });
    }

    /**
     * @return void
     */
    public function scopeOrderByStatus(Builder $query)
    {
        $query->orderBy(DB::raw('status = "'.self::IN_PROGRESS_STATUS.'"'), 'desc')
            ->orderBy(DB::raw('status = "'.self::OPEN_STATUS.'"'), 'desc')
            ->orderBy(DB::raw('status = "'.self::COMPLETED_STATUS.'"'), 'desc');
    }
}
