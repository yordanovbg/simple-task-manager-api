<?php

namespace App\Models\Users;

use Database\Factories\UserFactory;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable;
    use Authorizable;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name', 'surname', 'email', 'task_manager',
    ];

    protected $casts = [
        'task_manager' => 'boolean',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var string[]
     */
    protected $hidden = [
        'password',
    ];

    // Model dependencies
    protected static function newFactory()
    {
        return UserFactory::new();
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    // Mutators
    /**
     * @return string
     */
    public function getFullnameAttribute()
    {
        return $this->name.' '.$this->surname;
    }

    /**
     * Hash password.
     *
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    // Scopes

    public function scopeTaskManagers(Builder $query): void
    {
        $query->where('task_manager', true);
    }

    public function scopeRegulars(Builder $query): void
    {
        $query->where('task_manager', false);
    }

    // Callable methods

    public function isTaskManager(): bool
    {
        return $this->task_manager;
    }
}
