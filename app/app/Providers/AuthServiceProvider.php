<?php

namespace App\Providers;

use App\Models\Tasks\Task;
use App\Policies\Tasks\TaskPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Gate::policy(Task::class, TaskPolicy::class);
    }
}
