<?php

namespace App\Providers;

use Illuminate\Cache\CacheManager;
use Illuminate\Contracts\Cookie\QueueingFactory;
use Illuminate\Cookie\CookieServiceProvider;
use Illuminate\Support\ServiceProvider;

class ComponentsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->setSingletons();
        $this->setBindings();
        $this->setAliases();
    }

    private function setSingletons()
    {
        $this->app->singleton('cookie', function () {
            return $this->app->loadComponent('session', CookieServiceProvider::class, 'cookie');
        });
    }

    private function setBindings()
    {
        $this->app->bind(QueueingFactory::class, 'cookie');
    }

    private function setAliases()
    {
        $this->app->alias('cache', CacheManager::class);
    }
}
