<?php

namespace App\Providers;

use App\Http\Requests\FormRequest;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    public function boot()
    {
        /*
       * We create a form,
       * init Container (App)
       * init Validator (Lumen Validator)
       * init Data (From Request)
       */
        $this->app->resolving(FormRequest::class, function ($form, $app) {
            $form = FormRequest::createFrom($app['request'], $form);
            $form->initData();
        });

        /*
         * When we already resolved the FormRequest
         * We call validate,
         * This will throw validation error or just pass
         * We are using that with dependencies injection
         */
        $this->app->afterResolving(FormRequest::class, function (FormRequest $formRequest) {
            $formRequest->validate();
        });
    }
}
