<?php

namespace App\Services\Tasks;

use App\Models\Tasks\Task;

abstract class TaskService
{
    protected array $data;

    protected ?Task $task;

    public function __construct(array $data, ?Task $task = null)
    {
        $this->data = $data;
        $this->task = $task ?? new Task();
    }

    public function save(): Task
    {
        $this->handleAssigneeFields()
            ->handleCreatorField();

        $this->task->fill($this->data);
        $this->task->save();

        return $this->task;
    }

    /**
     * @return $this
     */
    abstract protected function handleAssigneeFields(): self;

    /**
     * @return $this
     */
    abstract protected function handleCreatorField(): self;
}
