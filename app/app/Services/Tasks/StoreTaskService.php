<?php

namespace App\Services\Tasks;

use Carbon\Carbon;

class StoreTaskService extends TaskService
{
    protected function handleAssigneeFields(): TaskService
    {
        if (! auth()->user()->isTaskManager()) {
            $this->data['assignee_id'] = auth()->id();
        }

        $this->data['assigned_at'] = isset($this->data['assignee_id'])
            ? Carbon::now()
            : null;

        return $this;
    }

    protected function handleCreatorField(): TaskService
    {
        $this->data['creator_id'] = auth()->id();

        return $this;
    }
}
