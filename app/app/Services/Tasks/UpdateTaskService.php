<?php

namespace App\Services\Tasks;

use Carbon\Carbon;
use function is_null;

class UpdateTaskService extends TaskService
{
    protected function handleAssigneeFields(): TaskService
    {
        if (! auth()->user()->isTaskManager()) {
            return $this;
        }

        if (
            isset($this->data['assignee_id']) &&
            $this->data['assignee_id'] != $this->task->assigne_id
        ) {
            $this->data['assigned_at'] = isset($this->data['assignee_id'])
                ? Carbon::now()
                : null;
        } elseif (! isset($this->data['assignee_id']) || is_null($this->data['assignee_id'])) {
            $this->data['assigned_at'] = null;
            $this->data['assignee_id'] = null;
        }

        return $this;
    }

    protected function handleCreatorField(): TaskService
    {
        return $this;
    }
}
