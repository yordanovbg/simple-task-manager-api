<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\Request;

abstract class FormRequest extends Request
{
    /**
     * @var Validator
     */
    protected $validator;
    /**
     * @var array
     */
    protected $data;

    public function initData(): FormRequest
    {
        $this->data = $this->all();

        return $this;
    }

    protected function statusCode(): int
    {
        return 422;
    }

    protected function errorMessage(): string
    {
        return 'The given data is invalid';
    }

    protected function errorResponse(): JsonResponse
    {
        return response()->json([
            'message' => $this->errorMessage(),
            'errors' => $this->validator->errors()->messages(),
        ], $this->statusCode());
    }

    /**
     * Call the method before validator make
     * Here you can manipulate the data or something like that.
     */
    protected function beforeValidate(): void
    {
    }

    /**
     * @throws ValidationException
     */
    public function validate(): void
    {
        $this->beforeValidate();
        $this->validator = Validator::make(
            $this->data,
            $this->rules(),
            $this->messages()
        );

        if ($this->validator->fails()) {
            $this->validationFailed();
        }

        $this->validationPassed();
    }

    /**
     * @throws ValidationException
     */
    public function validated(): array
    {
        return $this->validator->validated();
    }

    /**
     * @throws ValidationException
     */
    protected function validationFailed(): void
    {
        throw new ValidationException($this->validator, $this->errorResponse());
    }

    /**
     * Call after validation passed, you can add custom logic here.
     */
    protected function validationPassed(): void
    {
    }

    protected function messages(): array
    {
        return [];
    }

    /**
     * @return array
     *               The request must fill his own rules
     *               Compatible with all Lumen/Laravel validation rules
     */
    abstract protected function rules(): array;
}
