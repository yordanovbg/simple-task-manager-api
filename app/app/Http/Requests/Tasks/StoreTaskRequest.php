<?php

namespace App\Http\Requests\Tasks;

use App\Http\Requests\FormRequest;
use App\Models\Tasks\Task;
use Illuminate\Validation\Rule;

class StoreTaskRequest extends FormRequest
{
    protected function rules(): array
    {
        $rules = [
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'status' => Rule::in([Task::OPEN_STATUS, TASK::IN_PROGRESS_STATUS]),
        ];

        $rules = $this->validateAssignee($rules);

        return $rules;
    }

    private function validateAssignee(array $rules): array
    {
        if (auth()->user()->isTaskManager()) {
            $rules['assignee_id'] = 'nullable|exists:users,id';
        }

        return $rules;
    }
}
