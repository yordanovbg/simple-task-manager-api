<?php

namespace App\Http\Transformers\Tasks;

use App\Models\Tasks\Task;
use League\Fractal\TransformerAbstract;

class TaskTransformer extends TransformerAbstract
{
    public function transform(Task $task): array
    {
        return [
            'id' => $task->id,
            'title' => $task->title,
            'description' => $task->description,
            'status' => $task->status,
            'assignee' => $task->assigned_at
                ? $task->assignee->fullname
                : null,
            'assigned_at' => $task->assigned_at,
            'creator' => $task->creator->fullname,
            'created_at' => $task->created_at,
            'updated_at' => $task->updated_at,
        ];
    }
}
