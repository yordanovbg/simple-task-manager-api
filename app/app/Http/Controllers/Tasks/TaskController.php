<?php

namespace App\Http\Controllers\Tasks;

use App\Http\Requests\Tasks\StoreTaskRequest;
use App\Http\Requests\Tasks\UpdateTaskRequest;
use App\Http\Transformers\Tasks\TaskTransformer;
use App\Models\Tasks\Task;
use App\Services\Tasks\StoreTaskService;
use App\Services\Tasks\UpdateTaskService;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller;

class TaskController extends Controller
{
    public function index(): JsonResponse
    {
        $tasks = Task::userVisible()
            ->orderByStatus()
            ->get();

        return fractal()
            ->collection($tasks)
            ->transformWith(new TaskTransformer())
            ->respond();
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(StoreTaskRequest $request): JsonResponse
    {
        $data = $request->validated();

        $taskService = new StoreTaskService($data);
        $task = $taskService->save();

        return fractal()
            ->item($task)
            ->transformWith(new TaskTransformer())
            ->respond();
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(UpdateTaskRequest $request, string $taskId): JsonResponse
    {
        $task = Task::findOrFail($taskId);

        $this->authorize('update', $task);

        $data = $request->validated();

        $taskService = new UpdateTaskService($data, $task);
        $task = $taskService->save();

        return fractal()
            ->item($task)
            ->transformWith(new TaskTransformer())
            ->respond();
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(string $taskId): JsonResponse
    {
        $task = Task::findOrFail($taskId);

        $this->authorize('delete', $task);

        $task->delete();

        return response()->json([]);
    }
}
