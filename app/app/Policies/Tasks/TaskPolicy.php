<?php

namespace App\Policies\Tasks;

use App\Models\Tasks\Task;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * @param $ability
     *
     * @return bool|void
     */
    public function before(User $user, $ability)
    {
        if ($user->isTaskManager()) {
            return true;
        }
    }

    public function update(User $user, Task $task): bool
    {
        return $task->assignee_id === $user->id;
    }

    public function delete(User $user, Task $task): bool
    {
        return $task->creator_id === $user->id
            && $task->assignee_id === $user->id;
    }
}
