<?php

namespace Tests\unit\Http\FormRequest;

use App\Http\Requests\FormRequest;
use Tests\TestCase;

class FormRequestTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->app->router->post('form-request', function (SimpleRequest $request) {
            return response()->json([
                'all' => $request->all(),
                'validated' => $request->validated(),
            ]);
        });
    }

    public function testFailedValidationResponse()
    {
        $response = $this->post('form-request', [
            'name' => 'name',
        ])->seeStatusCode(422)
            ->response->getData(true);

        $this->assertArrayHasKey('message', $response);
        $this->assertEquals('The given data is invalid', $response['message']);
        $this->assertArrayHasKey('errors', $response);
        $this->assertIsArray($response['errors']);
        $this->assertArrayHasKey('username', $response['errors']);
    }

    public function testPassedValidationResponse()
    {
        $this->post('form-request', [
            'username' => 'username',
        ])->seeStatusCode(200);
    }

    public function testValidateData()
    {
        $response = $this->post('form-request', [
            'username' => 'username',
            'name' => 'name',
        ])->response->getData(true);

        $validated = $response['validated'];
        $this->assertArrayHasKey('username', $validated);
        $this->assertArrayNotHasKey('name', $validated);
        $this->assertEquals('username', $validated['username']);
    }
}

class SimpleRequest extends FormRequest
{
    protected function rules(): array
    {
        return [
            'username' => 'required|string',
        ];
    }
}
