<?php

namespace Tests\Feature\Tasks;

use App\Models\Tasks\Task;
use App\Models\Users\User;
use Carbon\Carbon;
use Database\Seeders\UsersTableSeeder;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\TestCase;

class TaskControllerUpdateTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $usersSeeder = new UsersTableSeeder();
        $usersSeeder->run();
    }

    public function testTaskManagerAssignTask()
    {
        $taskManager = User::taskManagers()->first();
        $token = auth()->tokenById($taskManager->id);
        $task = Task::factory()->create([
            'assignee_id' => null,
            'assigned_at' => null,
        ]);

        $response = $this->put(
            route('api.tasks.update', ['taskId' => $task->id]),
            [
                'title' => 'New title',
                'description' => 'New description',
                'assignee_id' => $taskManager->id,
                'status' => Task::IN_PROGRESS_STATUS,
            ],
            ['Authorization' => 'Bearer '.$token]
        )->seeStatusCode(200);

        $task = $task->fresh();

        $response->seeJsonEquals([
            'data' => [
                'id' => $task->id,
                'title' => 'New title',
                'description' => 'New description',
                'status' => Task::IN_PROGRESS_STATUS,
                'assignee' => $taskManager->fullname,
                'assigned_at' => $task->assigned_at,
                'creator' => $task->creator->fullname,
                'created_at' => $task->created_at,
                'updated_at' => $task->updated_at,
            ],
        ]);
    }

    public function testTaskManagerRemoveAssignee()
    {
        $taskManager = User::taskManagers()->first();
        $token = auth()->tokenById($taskManager->id);
        $task = Task::factory()->create([
            'assignee_id' => $taskManager->id,
            'assigned_at' => Carbon::now(),
        ]);

        $response = $this->put(
            route('api.tasks.update', ['taskId' => $task->id]),
            [
                'title' => $task->title,
                'description' => $task->description,
            ],
            ['Authorization' => 'Bearer '.$token]
        )->seeStatusCode(200);

        $task = $task->fresh();

        $response->seeJsonEquals([
            'data' => [
                'id' => $task->id,
                'title' => $task->title,
                'description' => $task->description,
                'status' => $task->status,
                'assignee' => null,
                'assigned_at' => null,
                'creator' => $task->creator->fullname,
                'created_at' => $task->created_at,
                'updated_at' => $task->updated_at,
            ],
        ]);
    }

    public function testRegularUserCanUpdateAssignedTask()
    {
        $regularUser = User::regulars()->first();
        $token = auth()->tokenById($regularUser->id);
        $assignedAt = Carbon::now();
        $task = Task::factory()->create([
            'assignee_id' => $regularUser->id,
            'assigned_at' => $assignedAt,
        ]);

        $response = $this->put(
            route('api.tasks.update', ['taskId' => $task->id]),
            [
                'title' => 'New title',
                'description' => 'New description',
                'assignee_id' => null,
                'status' => Task::COMPLETED_STATUS,
            ],
            ['Authorization' => 'Bearer '.$token]
        )->seeStatusCode(200);

        $task = $task->fresh();

        $response->seeJsonEquals([
            'data' => [
                'id' => $task->id,
                'title' => 'New title',
                'description' => 'New description',
                'status' => Task::COMPLETED_STATUS,
                'assignee' => $regularUser->fullname,
                'assigned_at' => $task->assigned_at,
                'creator' => $task->creator->fullname,
                'created_at' => $task->created_at,
                'updated_at' => $task->updated_at,
            ],
        ]);
    }

    public function testRegularUserAuthorizationFails()
    {
        $regularUser = User::regulars()->first();
        $token = auth()->tokenById($regularUser->id);
        $task = Task::factory()->create();

        $this->put(
            route('api.tasks.update', ['taskId' => $task->id]),
            [
                'title' => 'New title',
                'description' => 'New description',
                'assignee_id' => null,
                'status' => Task::COMPLETED_STATUS,
            ],
            ['Authorization' => 'Bearer '.$token]
        )->seeStatusCode(403);
    }
}
