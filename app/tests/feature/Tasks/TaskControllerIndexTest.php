<?php

namespace Tests\Feature\Tasks;

use App\Models\Tasks\Task;
use App\Models\Users\User;
use Database\Seeders\UsersTableSeeder;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\TestCase;

class TaskControllerIndexTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $usersSeeder = new UsersTableSeeder();
        $usersSeeder->run();
    }

    public function testTaskManagerCanSeeAll()
    {
        $taskManager = User::taskManagers()->first();
        $token = auth()->tokenById($taskManager->id);
        $openTask = Task::factory()->create();
        $inProgressTask = Task::factory()->create([
            'status' => Task::IN_PROGRESS_STATUS,
        ]);
        $completedTask = Task::factory()->create([
            'status' => Task::COMPLETED_STATUS,
        ]);

        $response = $this->get(
            route('api.tasks.index'),
            ['Authorization' => 'Bearer '.$token]
        )->seeStatusCode(200);

        $response->seeJsonEquals([
            'data' => [
                [
                    'id' => $openTask->id,
                    'title' => $openTask->title,
                    'description' => $openTask->description,
                    'status' => $openTask->status,
                    'assignee' => $openTask->assignee->fullname,
                    'assigned_at' => $openTask->assigned_at,
                    'creator' => $openTask->creator->fullname,
                    'created_at' => $openTask->created_at,
                    'updated_at' => $openTask->updated_at,
                ],
                [
                    'id' => $inProgressTask->id,
                    'title' => $inProgressTask->title,
                    'description' => $inProgressTask->description,
                    'status' => $inProgressTask->status,
                    'assignee' => $inProgressTask->assignee->fullname,
                    'assigned_at' => $inProgressTask->assigned_at,
                    'creator' => $inProgressTask->creator->fullname,
                    'created_at' => $inProgressTask->created_at,
                    'updated_at' => $inProgressTask->updated_at,
                ],
                [
                    'id' => $completedTask->id,
                    'title' => $completedTask->title,
                    'description' => $completedTask->description,
                    'status' => $completedTask->status,
                    'assignee' => $completedTask->assignee->fullname,
                    'assigned_at' => $completedTask->assigned_at,
                    'creator' => $completedTask->creator->fullname,
                    'created_at' => $completedTask->created_at,
                    'updated_at' => $completedTask->updated_at,
                ],
            ],
        ]);
    }

    public function testRegularsCanSeeOnlyAssignedOrCreated()
    {
        $regularUser = User::regulars()->first();
        $token = auth()->tokenById($regularUser->id);
        Task::factory()->create();
        $inProgressTask = Task::factory()->create([
            'status' => Task::IN_PROGRESS_STATUS,
            'assignee_id' => $regularUser->id,
        ]);
        $completedTask = Task::factory()->create([
            'status' => Task::COMPLETED_STATUS,
            'creator_id' => $regularUser->id,
        ]);

        $response = $this->get(
            route('api.tasks.index'),
            ['Authorization' => 'Bearer '.$token]
        )->seeStatusCode(200);

        $response->seeJsonEquals([
            'data' => [
                [
                    'id' => $inProgressTask->id,
                    'title' => $inProgressTask->title,
                    'description' => $inProgressTask->description,
                    'status' => $inProgressTask->status,
                    'assignee' => $inProgressTask->assignee->fullname,
                    'assigned_at' => $inProgressTask->assigned_at,
                    'creator' => $inProgressTask->creator->fullname,
                    'created_at' => $inProgressTask->created_at,
                    'updated_at' => $inProgressTask->updated_at,
                ],
                [
                    'id' => $completedTask->id,
                    'title' => $completedTask->title,
                    'description' => $completedTask->description,
                    'status' => $completedTask->status,
                    'assignee' => $completedTask->assignee->fullname,
                    'assigned_at' => $completedTask->assigned_at,
                    'creator' => $completedTask->creator->fullname,
                    'created_at' => $completedTask->created_at,
                    'updated_at' => $completedTask->updated_at,
                ],
            ],
        ]);
    }
}
