<?php

namespace Tests\Feature\Tasks;

use App\Models\Tasks\Task;
use App\Models\Users\User;
use Database\Seeders\UsersTableSeeder;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\TestCase;

class TaskControllerStoreTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $usersSeeder = new UsersTableSeeder();
        $usersSeeder->run();
    }

    public function testTaskManagerNotAssignedTask()
    {
        $taskManager = User::taskManagers()->first();
        $token = auth()->tokenById($taskManager->id);
        $taskData = [
            'title' => 'Test title',
            'description' => 'Test description',
            'status' => Task::OPEN_STATUS,
        ];

        $response = $this->post(
            route('api.tasks.store'),
            $taskData,
            ['Authorization' => 'Bearer '.$token]
        )->seeStatusCode(200);

        $createdTask = Task::first();

        $response->seeJsonEquals([
            'data' => [
                'id' => $createdTask->id,
                'title' => 'Test title',
                'description' => 'Test description',
                'status' => Task::OPEN_STATUS,
                'assignee' => null,
                'assigned_at' => null,
                'creator' => $taskManager->fullname,
                'created_at' => $createdTask->created_at,
                'updated_at' => $createdTask->updated_at,
            ],
        ]);
    }

    public function testTaskManagerAssignedTask()
    {
        $taskManager = User::taskManagers()->first();
        $assignee = User::regulars()->first();
        $token = auth()->tokenById($taskManager->id);
        $taskData = [
            'title' => 'Test title',
            'description' => 'Test description',
            'status' => Task::OPEN_STATUS,
            'assignee_id' => $assignee->id,
        ];

        $response = $this->post(
            route('api.tasks.store'),
            $taskData,
            ['Authorization' => 'Bearer '.$token]
        )->seeStatusCode(200);

        $createdTask = Task::first();

        $response->seeJsonEquals([
            'data' => [
                'id' => $createdTask->id,
                'title' => 'Test title',
                'description' => 'Test description',
                'status' => Task::OPEN_STATUS,
                'assignee' => $assignee->fullname,
                'assigned_at' => $createdTask->assigned_at,
                'creator' => $taskManager->fullname,
                'created_at' => $createdTask->created_at,
                'updated_at' => $createdTask->updated_at,
            ],
        ]);
    }

    public function testRegularUserAssignedSelfTask()
    {
        $assignee = User::regulars()->first();
        $token = auth()->tokenById($assignee->id);
        $taskData = [
            'title' => 'Test title',
            'description' => 'Test description',
            'status' => Task::OPEN_STATUS,
        ];

        $response = $this->post(
            route('api.tasks.store'),
            $taskData,
            ['Authorization' => 'Bearer '.$token]
        )->seeStatusCode(200);

        $createdTask = Task::first();

        $response->seeJsonEquals([
            'data' => [
                'id' => $createdTask->id,
                'title' => 'Test title',
                'description' => 'Test description',
                'status' => Task::OPEN_STATUS,
                'assignee' => $assignee->fullname,
                'assigned_at' => $createdTask->assigned_at,
                'creator' => $assignee->fullname,
                'created_at' => $createdTask->created_at,
                'updated_at' => $createdTask->updated_at,
            ],
        ]);
    }
}
