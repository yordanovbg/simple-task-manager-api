<?php

namespace Tests\Feature\Tasks;

use App\Models\Tasks\Task;
use App\Models\Users\User;
use Carbon\Carbon;
use Database\Seeders\UsersTableSeeder;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\TestCase;

class TaskControllerDestroyTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $usersSeeder = new UsersTableSeeder();
        $usersSeeder->run();
    }

    public function testTaskManagerCanAtAll()
    {
        $taskManager = User::taskManagers()->first();
        $token = auth()->tokenById($taskManager->id);
        $task = Task::factory()->create([
            'assignee_id' => null,
            'assigned_at' => null,
        ]);

        $this->delete(
            route('api.tasks.destroy', ['taskId' => $task->id]),
            [],
            ['Authorization' => 'Bearer '.$token]
        )->seeStatusCode(200);
    }

    public function testRegularCanOnlyIfIsCreatorAndAssignee()
    {
        $regularUser = User::regulars()->first();
        $token = auth()->tokenById($regularUser->id);
        $task = Task::factory()->create([
            'assignee_id' => $regularUser->id,
            'assigned_at' => Carbon::now(),
            'creator_id' => $regularUser->id,
        ]);

        $this->delete(
            route('api.tasks.destroy', ['taskId' => $task->id]),
            [],
            ['Authorization' => 'Bearer '.$token]
        )->seeStatusCode(200);
    }

    public function testRegularCanNotAsCreatorAndNotAssignee()
    {
        $regularUser = User::regulars()->first();
        $token = auth()->tokenById($regularUser->id);
        $task = Task::factory()->create([
            'creator_id' => $regularUser->id,
        ]);

        $this->delete(
            route('api.tasks.destroy', ['taskId' => $task->id]),
            [],
            ['Authorization' => 'Bearer '.$token]
        )->seeStatusCode(403);
    }

    public function testRegularCanNotAsNotCreatorAndAsAssignee()
    {
        $regularUser = User::regulars()->first();
        $token = auth()->tokenById($regularUser->id);
        $task = Task::factory()->create([
            'assignee_id' => $regularUser->id,
            'assigned_at' => Carbon::now(),
        ]);

        $this->delete(
            route('api.tasks.destroy', ['taskId' => $task->id]),
            [],
            ['Authorization' => 'Bearer '.$token]
        )->seeStatusCode(403);
    }
}
