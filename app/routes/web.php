<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['namespace' => 'Auth', 'prefix' => 'auth', 'as' => 'auth'], function () use ($router) {
    $router->post('login', ['uses' => 'LoginController@login', 'as' => 'login']);
});

$router->group(['namespace' => 'Tasks', 'prefix' => 'tasks', 'as' => 'tasks', 'middleware' => ['auth']], function () use ($router) {
    $router->get('/', ['uses' => 'TaskController@index', 'as' => 'index']);
    $router->post('/', ['uses' => 'TaskController@store', 'as' => 'store']);
    $router->put('/{taskId}', ['uses' => 'TaskController@update', 'as' => 'update']);
    $router->delete('/{taskId}', ['uses' => 'TaskController@destroy', 'as' => 'destroy']);
});
