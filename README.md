# Simple task manager
* API built app
### Tech stack
* Docker
* Nginx
* MySql
* Php
* Lumen

## Installation
* Pull repo
* go inside a folder and run `docker-compose up -d --build`
* Create two databases `app` and `app_test`
* `.env` contains all of the need credentials. Usually we put in `.gitignore`, but only for demo I leave it here
* Run `docker exec -ti php8-container bash` 
* Make sure you are inside `/var/www/app`
* Run `composer install`
* Run `php artisan migrate`
* Run `php artisan db:seed`


## Features
* Import `Next basket.postman_collectio.json` into Postman to see list of endpoints

### User authentication and authorization
* JWT Token is used to secure the endpoints
* Login with email and password. Open `app/database/seeders/UsersTableSeeder.php` to see the credentials
* We have two types of users: `Task manager` and `Regular`


### Tasks
* Required fields: `title`, `description`, `status`
* Has statuses: `Open`, `In progress`, `Completed`.
* Can be assigned to the user

#### Create task
* Task manager can create and assign task for every user.
* Regular user can create, but can not assign the task. It is assigned automatically to current user
* Available statuses on create: `Open`, `In progress`

#### Update task
* Task manager can update all the tasks, assign and remove assigned user
* Regular user can update only `Ttile`, `Description` and `Status`. He can update only tasks where is assigned on
* All tasks statuses area available

#### Delete task
* Task manage can delete all
* Regular user can delete only if is creator and assigned on

#### Index page (View all)
* Tasks are ordered by status (`In progress`, `Open`, `Completed`)
* Task manager can see all of them
* Regular user can see only if is creator or assigned on

## Tests
* Go inside docker container. Make sure you are in `/var/www/app`. Run `vendor/bin/phpunit`
* There is two type of tests: `Unit` and `Feature` tests
